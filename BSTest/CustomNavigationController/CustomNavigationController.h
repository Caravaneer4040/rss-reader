//
//  CustomNavigationController.h
//  Quikoo
//
//  Created by Dmitry Vorobyov on 1/27/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
