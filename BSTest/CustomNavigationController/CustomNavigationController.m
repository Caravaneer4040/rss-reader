//
//  CustomNavigationController.m
//  Quikoo
//
//  Created by Dmitry Vorobyov on 1/27/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import "CustomNavigationController.h"
#import "AppDelegate.h"
@interface CustomNavigationController ()

@end

@implementation CustomNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationBar.barTintColor = [UIColor colorWithRed:68/255.0 green:170/255.0 blue:119/255.0 alpha:1];
    
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor whiteColor],NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationBar.barTintColor = [UIColor colorWithRed:68/255.0 green:170/255.0 blue:119/255.0 alpha:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
