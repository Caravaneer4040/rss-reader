//
//  WebPlayerVC.m
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/28/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import "WebPlayerVC.h"

@interface WebPlayerVC ()

@end

@implementation WebPlayerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [webView loadRequest:requestObj];
    webView.scrollView.bounces = NO;
    [webView setOpaque:NO];
    
    webView.backgroundColor = [UIColor blackColor];
    
    activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = self.view.center;
    [self.view addSubview:activityIndicator];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [activityIndicator startAnimating];
    return YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [activityIndicator stopAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [activityIndicator stopAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
