//
//  DetailsVC.m
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/28/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import "DetailsVC.h"
#import "WebPlayerVC.h"

@interface DetailsVC ()

@end

@implementation DetailsVC
@synthesize entry;

- (void)viewDidLoad {
    [super viewDidLoad];
    files = [[entry objectForKey:@"media:group"] objectForKey:@"media:content"];

    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    name.attributedText = [[NSAttributedString alloc] initWithString:[entry objectForKey:@"title"]
                                                             attributes:underlineAttribute];
    [summary setText:[entry objectForKey:@"description"]];
    
    NSDateFormatter *formater = [NSDateFormatter new];
    [formater setTimeStyle:NSDateFormatterMediumStyle];
    [formater setDateStyle:NSDateFormatterMediumStyle];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZZ"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];

    NSDate *pubDate = [dateFormatter dateFromString:[entry objectForKey:@"pubDate"]];
    NSString *dateString = [formater stringFromDate:pubDate];
    [date setText:dateString];

}

- (IBAction)openURL:(id)sender
{
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[entry objectForKey:@"link"]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark TableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSDictionary *enclosure = [files objectAtIndex:indexPath.row];
    NSURL* url=[NSURL URLWithString:[enclosure objectForKey:@"_url"]];
    NSString* last=[url lastPathComponent];
    [cell.textLabel setText:last];
    [cell.detailTextLabel setText:[NSString stringWithFormat:@"%.2f Kb",[[enclosure objectForKey:@"_fileSize"]floatValue]/1024]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return files.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"openFile" sender:[[files objectAtIndex:indexPath.row] objectForKey:@"_url"]];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Files";
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[WebPlayerVC class]])
    {
        ((WebPlayerVC*)segue.destinationViewController).urlString = sender;
    }
}

@end
