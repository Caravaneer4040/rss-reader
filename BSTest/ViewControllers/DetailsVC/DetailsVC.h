//
//  DetailsVC.h
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/28/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsVC : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UILabel *name;
    IBOutlet UITextView *summary;
    IBOutlet UILabel *date;
    NSArray *files;
}
@property NSDictionary *entry;
@end
