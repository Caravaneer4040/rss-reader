//
//  EntriesList.h
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/27/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface EntriesListVC : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *allEntries;
    IBOutlet UITableView *entriesList;
}
@property UIRefreshControl *refreshControl;

@end
