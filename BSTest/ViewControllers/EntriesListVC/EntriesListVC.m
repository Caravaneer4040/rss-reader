//
//  EntriesList.m
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/27/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import "EntriesListVC.h"
#import "EntriesCell.h"
#import "DetailsVC.h"
#import "XMLDictionary.h"

@interface EntriesListVC ()

@end

@implementation EntriesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    allEntries = [NSMutableArray array];
    [self addPullToRefresh];
    [self getTEDentries];
}

-(void)addPullToRefresh
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor colorWithRed:68/255.0 green:170/255.0 blue:119/255.0 alpha:1];
    [self.refreshControl addTarget:self
                            action:@selector(updateInBackground)
                  forControlEvents:UIControlEventValueChanged];
    [entriesList addSubview:self.refreshControl];
}

-(void)updateInBackground
{
    [self performSelectorInBackground:@selector(getTEDentries) withObject:nil];
}

-(void)getTEDentries
{
    NSURL *URL = [[NSURL alloc] initWithString:@"http://www.ted.com/themes/rss/id/6"];
    NSString *xmlString = [[NSString alloc] initWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:NULL];
    if (!xmlString)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please check your internet connection or try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        [self.refreshControl endRefreshing];
        return;
    }
    NSDictionary *xmlDoc = [NSDictionary dictionaryWithXMLString:xmlString];
    NSDictionary *channel = [xmlDoc objectForKey:@"channel"];
    self.title = [channel objectForKey:@"title"];
    allEntries = [[[channel objectForKey:@"item"] reverseObjectEnumerator] allObjects];

    //allEntries = [[NSMutableArray alloc] initWithArray:[channel objectForKey:@"item"]];
    [self.refreshControl endRefreshing];
    [entriesList reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[DetailsVC class]])
    {
        UITableViewCell *cell = sender;
        NSIndexPath *ip = [entriesList indexPathForCell:cell];
        ((DetailsVC*)segue.destinationViewController).entry = [allEntries objectAtIndex:ip.row];
    }
}

#pragma mark TableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    EntriesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
        cell = [[EntriesCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    NSDictionary *entry = [allEntries objectAtIndex:indexPath.row];
    cell.titleLabel.text = [entry objectForKey:@"title"];

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZZ"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    NSDate *pubDate = [dateFormatter dateFromString:[entry objectForKey:@"pubDate"]];
    NSDateFormatter *formater = [[NSDateFormatter alloc]init];
    [formater setTimeStyle:NSDateFormatterShortStyle];
    [formater setDateStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formater stringFromDate:pubDate];

    NSArray *enclosures = [[entry objectForKey:@"media:group"] objectForKey:@"media:content"];
    cell.dateLabel.text = [NSString stringWithFormat:@"%@ - %@  Files: %lu", dateString ,[self dateDiff:pubDate],(unsigned long)[enclosures count]];
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allEntries count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSString *)dateDiff:(NSDate *)origDate {
    NSDate *convertedDate = origDate;
    NSDate *todayDate = [NSDate date];
    double ti = [convertedDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 1) {
        return @"never";
    } else 	if (ti < 60) {
        return @"less than a minute ago";
    } else if (ti < 3600) {
        int diff = round(ti / 60);
        return [NSString stringWithFormat:@"%d minutes ago", diff];
    } else if (ti < 86400) {
        int diff = round(ti / 60 / 60);
        return[NSString stringWithFormat:@"%d hours ago", diff];
    } else {
        int diff = round(ti / 60 / 60 / 24);
        return[NSString stringWithFormat:@"%d days ago", diff];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
