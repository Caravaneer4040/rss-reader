//
//  EntriesCell.h
//  BSTest
//
//  Created by Dmitry Vorobyov on 4/28/15.
//  Copyright (c) 2015 Dmitry Vorobyov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EntriesCell : UITableViewCell
@property IBOutlet UILabel *titleLabel;
@property IBOutlet UILabel *dateLabel;
@end
